package bo.digicert.cryptoexamplesandroid

import java.security.MessageDigest

class HashUtil {
    companion object {
        fun calculateHash(source: ByteArray): String {
            val digest = MessageDigest.getInstance("SHA-256")
            val hash = digest.digest(source)
            val hexString = StringBuffer()
            for (element in hash) {
                val hex = Integer.toHexString(0xFF and element.toInt())
                if (hex.length == 1) {
                    hexString.append('0')
                }
                hexString.append(hex)
            }
            return hexString.toString()
        }
    }
}