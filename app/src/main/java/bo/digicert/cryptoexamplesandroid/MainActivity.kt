package bo.digicert.cryptoexamplesandroid

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private var fileUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Digicert - ${getString(R.string.app_name)} "
        btnSelectFile.setOnClickListener {
            pickFile()
        }

        btnCalculateHash.setOnClickListener {
            calculateHash()
        }
    }

    private fun calculateHash() {
        if (fileUri == null) {
            Toast.makeText(this, "No hay archivo seleccionado", Toast.LENGTH_LONG).show()
        }
        val fileBytes = readBytesFromUri(fileUri!!)
        val hash = HashUtil.calculateHash(fileBytes!!)
        tvhash.visibility = View.VISIBLE
        tvhash.text = hash
        Toast.makeText(this, "SHA256 calculado", Toast.LENGTH_LONG).show()
    }

    private fun readBytesFromUri(uri: Uri): ByteArray? {
        return contentResolver.openInputStream(uri)?.buffered()?.use { it.readBytes() }
    }

    private fun pickFile() {
        var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.type = "*/*"
        chooseFile = Intent.createChooser(chooseFile, "Choose a file")
        startActivityForResult(chooseFile, PICKFILE_RESULT_CODE)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PICKFILE_RESULT_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    fileUri = data!!.data
                    if (fileUri != null) {
                        tvFilename.text = File(fileUri!!.path!!).name
                        btnCalculateHash.isEnabled = true
                    }
                    return
                }
                tvFilename.text = "Selecciona una archivo"
                fileUri = null
                btnCalculateHash.isEnabled = false
                tvhash.visibility = View.GONE
            }
        }
    }

    companion object {
        const val PICKFILE_RESULT_CODE: Int = 1
    }
}
